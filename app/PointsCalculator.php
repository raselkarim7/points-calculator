<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointsCalculator extends Model
{
    //

    protected $fillable = [
        'name', 'email', 'contact', 'occupationfield', 'agerange',
        'ieltsscore','expryears','eduqualification','points' ,'worked_in_au','naati_exam','spouse_skill','cvfile'
    ];

    public static  function forceCreate($array)
    {

    }

}
