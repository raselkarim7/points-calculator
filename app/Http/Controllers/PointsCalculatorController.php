<?php

namespace App\Http\Controllers;


use App\PointsCalculator;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate;


class PointsCalculatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pointscal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
       // print_r($_FILES);
        $this->validate( $request,
                    [
                        'name' => 'required',
                        'email' => 'required|unique:points_calculators,email',
                        'contact' => 'required',
                        'occupationfield' => 'required',
                        'agerange' => 'required',
                        'ieltsscore' => 'required',
                        'expryears' => 'required',
                        'eduqualification' => 'required',
                    ]);

            //dd($request->cvfile);
            //$request->file('cvfile');

            if($request->hasFile('cvfile'))
            {
                    $cvfile = $request->file('cvfile');
                    $cvfile_rename = time().'.'.$cvfile->getClientOriginalExtension();
                    $cvfile->move(public_path('uploadedcv'),$cvfile_rename);
            }else{
                $cvfile_rename = 'No file uploaded';
            }

       $fault = PointsCalculator::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'contact' => $request['contact'],
            'occupationfield' => $request['occupationfield'],

            'agerange' => $request['agerange'],
            'ieltsscore' =>$request['ieltsscore'],
            'expryears' =>$request['expryears'],
            'eduqualification' =>$request['eduqualification'],

            'points' => $request['points'],
            'worked_in_au' =>$request['worked_in_au'],
            'naati_exam' =>$request['naati_exam'],
            'spouse_skill' =>$request['spouse_skill'],
            'cvfile' => $cvfile_rename

        ]);

        // return redirect('/');
        // return  redirect('/')->with(['message' => 'Project Created']);
          return ['message'=>'Project Created'];

    }

    public function getpeople(){
        $users = PointsCalculator::all();
        return view('people',compact('users'));
    }
    public function single($id){
        $showsingle = PointsCalculator::find($id);
        return view('single',compact('showsingle'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PointsCalculator  $pointsCalculator
     * @return \Illuminate\Http\Response
     */
    public function show(PointsCalculator $pointsCalculator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PointsCalculator  $pointsCalculator
     * @return \Illuminate\Http\Response
     */
    public function edit(PointsCalculator $pointsCalculator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PointsCalculator  $pointsCalculator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PointsCalculator $pointsCalculator)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PointsCalculator  $pointsCalculator
     * @return \Illuminate\Http\Response
     */
    public function destroy(PointsCalculator $pointsCalculator)
    {
        //
    }
}
