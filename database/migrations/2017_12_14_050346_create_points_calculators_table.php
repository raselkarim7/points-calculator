<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointsCalculatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points_calculators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->string('email',100);
            $table->string('contact',200);
            $table->string('occupationfield',200);
            $table->string('agerange',100);
            $table->string('ieltsscore',100);
            $table->string('expryears',100);
            $table->string('eduqualification',200);
            $table->integer('points');
            $table->text('worked_in_au')->nullable();
            $table->text('naati_exam')->nullable();
            $table->text('spouse_skill')->nullable();
            $table->string('cvfile',130)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points_calculators');
    }
}
