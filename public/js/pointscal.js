class Errors {
    constructor() {
        this.err = {};
    }

    get(field) {
        if (this.err[field]) {
            console.log('but why not here?? ');
            return this.err[field][0];
        }
    }

    record(toerr) {
        this.err = toerr.errors;
        //console.log(' here comes always');
        console.log('toerr = ');
        console.log(toerr);

    }

    clear(field){
        if(field) {
            delete this.err[field];
            return;
        }

        this.err = {};
    }

    has(filed){
        return this.err.hasOwnProperty(filed);
    }

    any(){
        return Object.keys(this.err).length > 0;
    }
}




class Form{

    constructor(data){

        this.originalData = data;

        console.log('Inside Form Constructor = ');
        console.log(data);
        console.log('Inside Form Constructor For LOOP = ');

        for(let field in data){
            this[field] = data[field];
            console.log( data.field );
        }

        this.errors = new Errors();
    }
    reset(){
        for ( let field in this.originalData){
            this[field] = '';
        }
    }
    data(){

        let data =   Object.assign({},this);
        delete data.originalData;
        delete data.errors;

        return data;

    }

    submit(requestType, url){
        axios[requestType](url, this.data())
            .then( this.onSuccess.bind(this))
            .catch(this.onFail.bind(this));

    }

    onSuccess(response){
        alert(response.data.message);
        this.errors.clear();
        this.reset();
    }

    onFail(error){
        this.errors.record(error.response.data);

    }
}




new Vue({

    el: '#root',

    data: {
        form: new Form({
            name: "",
            description: "",
            email: "",
            contact:"",
                        /* points are calculated on these 6 factors */
            agerange:"0",
            occupation:"0",
            IELTS:"0",
            experience:"0",
            education:"0",
            institution:"0",
            testedu:"0",

            worked_in_au:"",
            naati_exam:"",
            spouse:"",
        }),
        //
        educategories : {
            'Select Education Field': { 'Select at first Education Field': 0 },
            'Australian Institution': { 'Select ': 0, 'Australian University': 20, 'Aus High School': 10 },
            '7 Top Universities of BD': { 'Select ':0,'BUET': 20, 'KUET': 20, 'RUET': 20 },
            'Others': {'Select ': 0,'Masters': 15,'Hons. / Diploma': 10,'Under Graduate / HSC+': 0 },
        },
    },

    methods: {
        onSubmit(){
            this.form.submit('post','/projects');
        },

        funEducation(){
                //console.log(this.form.education);
                var eduOne = this.form.education;
                var lcns = this.educategories[eduOne]/* || []*/;
                      console.log(eduOne);
                      console.log(lcns);
                var i=0;
                var subarray = [];
                for (var key in lcns) {
                    if (lcns.hasOwnProperty(key)) {
                       // console.log(key + " -> " + lcns[key]);
                        subarray[i]  =  '<option value="' + lcns[key] + '">' + key + '</option>' ;
                        i++;
                    }
                }
                var str = subarray.join('');
                      console.log('that string = ');
                      console.log(str);
                document.getElementById('educategory').innerHTML = str;

                delete str;
                delete lcns;
                delete subarray;
                delete eduOne;
          // return str;
         /*       var html = $.map(lcns, function(lcn,key){
                    console.log(lcn);
                    return '<option value="' + lcn + '">' + key + '</option>'
                }).join('');

                document.getElementById('educategory').innerHTML = html*/
        }
    },


});