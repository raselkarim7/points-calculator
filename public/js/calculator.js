/*--------------------------------- occupation -------------------------------*/
Vue.component('occupation',{
	template: ` 
	<div>
	<select v-model="occupationpoint"   v-on:change="passEventByFun(occupationpoint)" class="form-control">
		<option disabled value="">Select Your Occupation.</option>
		<option v-for="opt in options" v-bind:value=opt> {{ opt.text }}</option>
	</select>
	<!-- <p> Correspoing value of selection = 	{{ occupationpoint }} </p> -->
	</div>   
	`, 
	data(){
		return {  
			options:[
                { text:"Construction Managers", value:'0' },
                { text:"Engineering Managers", value:'0' },
                { text:"Child Care Centre Managers", value:'0' },
                { text:"Health and Welfare Services Managers", value:'0' },
                { text:"Accountants*", value:'0' },
                { text:"Auditors, Company Secretaries and Corporate Treasurers*", value:'0' },
                { text:"Actuaries, Mathematicians and Statisticians", value:'0' },
                { text:"Land Economists and Valuers", value:0 },
                { text:"Architects and Landscape Architects", value:0 },
                { text:"Cartographers and Surveyors", value:0 },
                { text:"Chemical and Materials Engineers", value:0 },
                { text:"Civil Engineering Professionals", value:0 },
                { text:"Electrical Engineers", value:0 },
                { text:"Electronics Engineers*", value:0 },
                { text:"Industrial, Mechanical and Production Engineers*", value:0 },
                { text:"Other Engineering Professionals*", value:0 },
                { text:"Agricultural and Forestry Scientists", value:0 },
                { text:"Medical Laboratory Scientists", value:0 },
                { text:"Veterinarians", value:0 },
                { text:"Other Natural and Physical Science Professionals", value:0 },
                { text:"Early Childhood (Pre-primary School) Teachers", value:0 },
                { text:"Secondary School Teachers", value:0 },
                { text:"Special Education Teachers", value:0 },
                { text:"Medical Imaging Professionals", value:0 },
                { text:"Optometrists and Orthoptists", value:0 },
                { text:"Other Health Diagnostic and Promotion Professionals", value:0 },
                { text:"Chiropractors and Osteopaths", value:0 },
                { text:"Occupational Therapists", value:0 },
                { text:"Physiotherapists", value:0 },
                { text:"Podiatrists", value:0 },
                { text:"Speech Professionals and Audiologists", value:0 },
                { text:"General Practitioners and Resident Medical officers", value:0 },
                { text:"Internal Medicine Specialists", value:0 },
                { text:"Psychiatrists", value:0 },
                { text:"Surgeons", value:0 },
                { text:"Other Medical Practitioners", value:0 },
                { text:"Midwives", value:0 },
                { text:"Registered Nurses", value:0 },
                { text:"ICT Business and Systems Analysts*", value:0 },
                { text:"Software and Applications Programmers*", value:0 },
                { text:"Database and Systems Administrators and ICT Security Specialists", value:0 },
                { text:"Computer Network Professionals*", value:0 },
                { text:"Telecommunications Engineering Professionals", value:0 },
                { text:"Barristers", value:0 },
                { text:"Solicitors", value:0 },
                { text:"Psychologists", value:0 },
                { text:"Social Workers", value:0 },
                { text:"Civil Engineering Draftspersons and Technicians", value:0 },
                { text:"Electrical Engineering Draftspersons and Technicians", value:0 },
                { text:"Telecommunications Technical Specialists", value:0 },
                { text:"Automotive Electricians", value:0 },
                { text:"Motor Mechanics", value:0 },
                { text:"Sheetmetal Trades Workers", value:0 },
                { text:"Structural Steel and Welding Trades Workers", value:0 },
                { text:"Metal Fitters and Machinists", value:0 },
                { text:"Precision Metal Trades Workers", value:0 },
                { text:"Panelbeaters", value:0 },
                { text:"Bricklayers and Stonemasons", value:0 },
                { text:"Carpenters and Joiners", value:0 },
                { text:"Painting Trades Workers", value:0 },
                { text:"Glaziers", value:0 },
                { text:"Plasterers", value:0 },
                { text:"Wall and Floor Tilers", value:0 },
                { text:"Plumbers", value:0 },
                { text:"Electricians", value:0 },
                { text:"Airconditioning and Refrigeration Mechanics", value:0 },
                { text:"Electrical Distribution Trades Workers", value:0 },
                { text:"Electronics Trades Workers", value:0 },
                { text:"Chefs", value:0 },
                { text:"Cabinetmakers", value:0 },
                { text:"Boat Builders and Shipwrights", value:0 },
			],
			occupationpoint: '',
		}
	},
	methods: {
		passEventByFun: function(occupationpoint){
			console.log(' occupation portion,  occupationpoint: ');
			console.log(occupationpoint);
			this.$emit('occupationevent',occupationpoint);
		}
	}
});

/*--------------------------------- age-range -------------------------------*/
Vue.component('age-range',{
	template: ` 
	<div>
	<select v-model="agepoint" v-on:change="passEventByFun(agepoint)" class="form-control">
		<option disabled value="">Select Your Age Range</option>
		<option v-for="opt in options" v-bind:value=opt> {{ opt.text }}</option>
	</select>
	<!-- <p> Correspoing value of selection = 	{{ agepoint }} </p> -->
	</div>   
	`, 
	data(){
		return {  
			options:[
						{ text: '18-24 years',   value: '25' },
						{ text: '25-32 years',   value: '30' },
						{ text: '33-39 years',   value: '25' },
						{ text: '40-44 years',   value: '15' },
			],
			agepoint: '', 
		}
	},
	methods: {
		passEventByFun: function(agepoint){
			console.log(' age-range portion,  agepoint: ');
			console.log(agepoint);
			this.$emit('agerangeevent',agepoint);
		}
	}
});



/*----------------------------------ielts-----------------------------------*/
Vue.component('ielts', {
	template: ` 
	<div>
	<select v-model="ieltspoint" v-on:change="passEventByFun(ieltspoint)" class="form-control">
		<option disabled value="">IELTS Score All Band</option>
		<option v-for="opt in options" v-bind:value=opt> {{ opt.text }}</option>
	</select>
	<!-- <p> Correspoing value of selection = 	{{ ieltspoint }} </p> -->
	</div>   
	`, 
	data(){
		return {  
			options:[
						{ text: 'All Band Min 6.0',   value: '0' },
						{ text: 'All Band Min 7.0',   value: '10' },
						{ text: 'All Band Min 8.0',   value: '20' },
						
			],
			ieltspoint: '', 
		}
	},
	methods: {
		passEventByFun: function(ieltspoint){
			console.log(' IELTS portion,  ieltspoint: ');
			console.log(ieltspoint);
			this.$emit('ieltsevent',ieltspoint);
		}
	}
});

/*------------------------------ experience --------------------------------*/
Vue.component('experience', {
	template: ` 
	<div>
	<select v-model="exprpoint" v-on:change="passEventByFun(exprpoint)" class="form-control">
		<option disabled value="">Experience Time Range</option>
		<option v-for="opt in options" v-bind:value=opt> {{ opt.text }}</option>
	</select>
	<!-- <p> Correspoing value of selection = 	{{ exprpoint }} </p> -->
	</div>   
	`, 
	data(){
		return {  
			options:[
						{ text: 'Less than 3 years',  value: '0'},
						{ text: '3-4 years',   value: '5' },
						{ text: '5-7 years',   value: '10' },
						{ text: '8-10 years',  value: '15' },
						
			],
			exprpoint: '', 
		}
	},
	methods: {
		passEventByFun: function(exprpoint){
			console.log(' Experience portion,  exprspoint: ');
			console.log(exprpoint);
			this.$emit('experienceevent',exprpoint);
		}
	}
});


/*------------------------------ Education --------------------------------*/
Vue.component('education', {
	template: ` 
	<div>
	<select v-model="edupoint" v-on:change="passEventByFun(edupoint)" class="form-control">
		<option disabled value="">Highest Educational Level</option>
		<option v-for="opt in options" v-bind:value=opt> {{ opt.text }}</option>
	</select>
	<!-- <p> Correspoing value of selection = 	{{ edupoint }} </p> -->
	</div>   
	`, 
	data(){
		return {  
			options:[
						{ text: 'Diploma',    value: '10' },
						{ text: 'Masters',    value: '15' },
						{ text: 'Doctorate',  value: '20' },
						{ text: 'Bachelor',   value: '0' },
			],
			edupoint: '', 
		}
	},
	methods: {
		passEventByFun: function(edupoint){
			console.log(' Education portion,  edupoint: ');
			console.log(edupoint);
			this.$emit('educationevent',edupoint);
		}
	}
});


class Errors {
    constructor() {
        this.err = {};
    }

    get(field) {
    	console.log('must hereee :: So, this.err[field] =');
    //	console.log( this.err[field] );
        if (this.err[field]) {
            console.log('but why not here?? ');
            return this.err[field][0];
        }
    }

    record(toerr) {
       this.err = toerr.errors;

        //console.log(' here comes always');

		/*------ The Vital Portion for Laravel 5.4---, For 5.4  [ this.err = toerr] ----->>----->>---->>---->>----->>-
		 Check the console for seeing the error Object in axios response,
		 then modify this portion  to assign returned error object to this.......
		 FFFFFor Laravel 5.4_____________________

		 record(toerr) {
		 this.err = toerr;
		 console.log('toerr = ');
		 console.log(toerr);
		 }

		 */


        console.log('toerr = ');
        console.log(toerr.errors);

    }

    clear(field){
    	console.log('inside clear(field), field==');
    	console.log('this.err[field]=');
    	console.log(this.err[field]);

    	if(field) {
            delete this.err[field];
            return;

        }
        this.err = {};
    }

    has(filed){
        return this.err.hasOwnProperty(filed);
    }

    any(){
        return Object.keys(this.err).length > 0;
    }
}


new Vue({
	el: '#root',
	data: {
			/*----modal portion ----- */
        		showModal: false,
				showmessage:false,



			/*------------------------*/

			total: 0, 
			final_agepoint: 0,
			final_ieltspoint: 0, 
			final_exprpoint: 0,
			final_edupoint: 0,
			final_bachelorpoint: 0,


            final_occupationpoint:0, //it's only for clearing the bug.
			/* --- save in DB ---*/
				name: '',
				email: '',
				contact: '',
				occupation_name: '',

				agerange_text: '',
				ielts_text:'',
				experience_text: '',
				education_text:'',

				total_points: 0,
				worked_in_au: '',
				naati_exam: '',
				spouse_skill: '',
			/* --- save in DB ---*/

			bachelorpoint: "", 
			institutionlist: [],
        	errors: new Errors(),
            /*--------- file upload ----------*/
				cvfile:'',
				cvfilename:'',
				uploadfile: ''					 //-- -- -- -- -- -- -- -- -- Upload File  -- -- -- -- -- -- -- -- --

	},



	methods: {
        occupationFun: function(occupationpoint){

                this.final_occupationpoint = parseInt(occupationpoint.value);
				this.occupation_name = occupationpoint.text; //-----------------------------------------
				this.errors.clear('occupationfield');
                console.log('inside occupationFun, occupation_name==');
                console.log(this.occupation_name);

			},
			agerangeFun: function(agepoint){
				this.final_agepoint =  parseInt( agepoint.value);
				this.agerange_text = agepoint.text;
                this.errors.clear('agerange');
			},

			ieltsFun: function(ieltspoint){
				this.final_ieltspoint =  parseInt(ieltspoint.value);
				this.ielts_text = ieltspoint.text;
                this.errors.clear('ieltsscore');
			},

			experienceFun: function(exprpoint){
				this.final_exprpoint =  parseInt(exprpoint.value);
				this.experience_text = exprpoint.text;
				this.errors.clear('expryears');
			},


			educationFun: function(edupoint){
				console.log('Edu Point: ');
				console.log(edupoint);
				this.bachelorpoint = ""; // it's important ..s
				this.final_bachelorpoint = 0 ;
				this.final_edupoint =  parseInt( edupoint.value );
				this.education_text = edupoint.text;////////////////////////////////////////////////////
				this.institutionlist = [];
				this.errors.clear('eduqualification');
				if(edupoint.value==0){
								this.bachelorpoint = "";
								this.institutionlist = [
									{ text: "Bangladesh University of Engineering & Technology", value: 15 },
									{ text: "Chittagong University of Engineering & Technology", value: 15 },
									{ text: "Dhaka University", value: 15 },
									{ text: "Dhaka University of Engineering & Technology", value: 15 },
									{ text: "Khulna University of Engineering and Technology", value: 15 },
									{ text: "Rajshahi University", value: 15 },
									{ text: "Rajshahi University of Engineering & Technology", value: 15 },
									{ text: "University of Chittagong", value: 15 },

									{ text: "Ahsanullah University of Science and Technology", value: 10 },
									{ text: "America Bangladesh University", value: 10 },
									{ text: "American International University Bangladesh", value: 10 },
									{ text: "ASA University Bangladesh", value: 10 },
									{ text: "Asian University of Bangladesh", value: 10 },
									{ text: "Atish Dipankar University of Science & Technology", value: 10 },
									{ text: "Bangladesh Islami University", value: 10 },
									{ text: "Bangladesh University", value: 10 },
									{ text: "Bangladesh University of Business & Technology (BUBT)", value: 10 },
									{ text: "Bangladesh University of Health Sciences", value: 10 },
									{ text: "BGC Trust University Bangladesh, Chittagong", value: 10 },
									{ text: "BGMEA University of Fashion & Technology", value: 10 },
									{ text: "BRAC University", value: 10 },
									{ text: "Britannia University", value: 10 },
									{ text: "CCN University of Science & Technology", value: 10 },
									{ text: "Central Women's University", value: 10 },
									{ text: "Chittagong Independent University (CIU)", value: 10 },
									{ text: "City University", value: 10 },
									{ text: "Coxs Bazar International University", value: 10 },
									{ text: "Daffodil International University", value: 10 },
									{ text: "Darul Ihsan University", value: 10 },
									{ text: "Dhaka International University", value: 10 },
									{ text: "East Delta University , Chittagong", value: 10 },
									{ text: "East West University", value: 10 },
									{ text: "Eastern University", value: 10 },
									{ text: "European University of Bangladesh", value: 10 },
									{ text: "Exim Bank Agricultural University, Bangladesh", value: 10 },
									{ text: "Fareast International University", value: 10 },
									{ text: "Feni University", value: 10 },
									{ text: "First Capital University of the Bangladesh", value: 10 },
									{ text: "German University Bangladesh", value: 10 },
									{ text: "Gono Bishwabidyalay", value: 10 },
									{ text: "Green University of Bangladesh", value: 10 },
									{ text: "Hamdard University Bangladesh", value: 10 },
									{ text: "IBAIS University", value: 10 },
									{ text: "Independent University, Bangladesh", value: 10 },
									{ text: "International Islamic University, Chittagong", value: 10 },
									{ text: "International University of Business Agriculture & Technology", value: 10 },
									{ text: "Ishakha International University", value: 10 },
									{ text: "Khwaja Yunus Ali University", value: 10 },
									{ text: "Leading University, Sylhet", value: 10 },
									{ text: "Manarat International University", value: 10 },
									{ text: "Metropolitan University, Sylhet", value: 10 },
									{ text: "North Bengal International University", value: 10 },
									{ text: "North East University Bangladesh", value: 10 },
									{ text: "North South University", value: 10 },
									{ text: "North Western University, Khulna", value: 10 },
									{ text: "Northern University Bangladesh", value: 10 },
									{ text: "Port City International University", value: 10 },
									{ text: "Premier University, Chittagong", value: 10 },
									{ text: "Presidency University", value: 10 },
									{ text: "Prime University", value: 10 },
									{ text: "Primeasia University", value: 10 },
									{ text: "Queens University", value: 10 },
									{ text: "Rajshahi Science & Technology University (RSTU), Natore", value: 10 },
									{ text: "Ranada Prasad Shaha University", value: 10 },
									{ text: "Royal University of Dhaka", value: 10 },
									{ text: "Shanto Mariam University of Creative Technology", value: 10 },
									{ text: "Sheikh Fazilatunnesa Mujib University", value: 10 },
									{ text: "Sonargaon University", value: 10 },
									{ text: "Southeast University", value: 10 },
									{ text: "Southern University Bangladesh , Chittagong", value: 10 },
									{ text: "Stamford University, Bangladesh", value: 10 },
									{ text: "State University Of Bangladesh", value: 10 },
									{ text: "Sylhet International University, Sylhet", value: 10 },
									{ text: "The Millennium University", value: 10 },
									{ text: "The Peoples University of Bangladesh", value: 10 },
									{ text: "The University of Asia Pacific", value: 10 },
									{ text: "United International University", value: 10 },
									{ text: "University of Development Alternative", value: 10 },
									{ text: "University of Information Technology & Sciences", value: 10 },
									{ text: "University of Liberal Arts Bangladesh", value: 10 },
									{ text: "University of Science & Technology, Chittagong", value: 10 },
									{ text: "University of South Asia", value: 10 },
									{ text: "Uttara University", value: 10 },
									{ text: "Varendra University", value: 10 },
									{ text: "Victoria University of Bangladesh", value: 10 },
									{ text: "World University of Bangladesh", value: 10 },
									{ text: "Z.H SIKDER UNIVFRSITY OF SCIENCE & TECHNOLOGY", value: 10 },
									{ text: "Bangabandhu Sheikh Mujibur Rahman Maritime university, Bangladesh", value: 10 },
									{ text: "Bangabandhu Sheikh Mujib Medical University", value: 10 },
									{ text: "Bangabandhu Sheikh Mujibur Rahaman Science & Technology University", value: 10 },
									{ text: "Bangabandhu Sheikh Mujibur Rahman Agricultural University", value: 10 },
									{ text: "Bangladesh Agricultural University,Mymensingh", value: 10 },
									{ text: "Bangladesh Open University", value: 10 },
									{ text: "Bangladesh Textile University", value: 10 },
									{ text: "Bangladesh University of Professionals", value: 10 },
									{ text: "Barisal University", value: 10 },
									{ text: "Begum Rokeya University", value: 10 },
									{ text: "Chittagong Veterinary and Animal Sciences University", value: 10 },
									{ text: "Comilla University", value: 10 },
									{ text: "Hajee Mohammad Danesh Science & Technology University", value: 10 },
									{ text: "Islamic University", value: 10 },
									{ text: "Jagannath University", value: 10 },
									{ text: "Jahangirnagar University", value: 10 },
									{ text: "Jatiya Kabi Kazi Nazrul Islam University", value: 10 },
									{ text: "Jessore University of Science & Technology", value: 10 },
									{ text: "Khulna University", value: 10 },
									{ text: "Mawlana Bhashani Science & Technology University", value: 10 },
									{ text: "National University", value: 10 },
									{ text: "Noakhali Science & Technology University", value: 10 },
									{ text: "Pabna University of Science and Technology", value: 10 },
									{ text: "Patuakhali Science And Technology University", value: 10 },
									{ text: "Shahjalal University of Science & Technology", value: 10 },
									{ text: "Sher-e-Bangla Agricultural University", value: 10 },
									{ text: "Sylhet Agricultural University", value: 10 },
									{ text: "Asian University for Women", value: 10 },
									{ text: "Islamic University of Technology", value: 10 },
									{ text: "South Asian University", value: 10 },
									{ text: "Diploma Institute", value: 10 },
									{ text: "Medical College", value: 10 },
                                ];
								console.log('now hereeeeeeeeeeeeeee = ');								
					}
			},
			bachelorFun: function(){

				this.final_bachelorpoint = this.bachelorpoint.value;
				this.education_text = this.bachelorpoint.text;
			},

    /*------------ file uploading methods ---------------- */

            onFileChange:function(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                this.createFile(files[0]);
            },
            createFile: function(file) {
                //var image = new Image();
                var cvfile = new File([""],'cvfile');
                var reader = new FileReader();
                var vm = this;

                var fullPath = document.getElementById('file').value;
					this.uploadfile = document.querySelector("#file"); //-- -- -- -- -- -- -- -- -- Upload File  -- -- -- -- -- -- -- -- --
					console.log(' inside create file, uploadfile ==== ');
					console.log(this.uploadfile);
                	console.log(this.uploadfile.files[0]);

                if (fullPath) {
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                    if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                    }
                    this.cvfilename = filename;
                }

                reader.onload = (e) => {
                    vm.cvfile = e.target.result;
                   // console.log('inside reader.onload ======= ');
                   // console.log(vm.cvfile);
                };
                reader.readAsDataURL(file);
            },
            removeFile: function (e) {
                this.cvfile = '';
                this.uploadfile='';

            },
        // Get the filename after uploading and place it inside p ---tag


        /*----------------------------------------------------*/

			onSubmit: function () {

                console.log('inside onSubmit = = = = ');
                //console.log('this.cvfile == ');
			    //console.log(this.cvfile);


            this.total_points = this.final_agepoint + this.final_ieltspoint +
               			        this.final_exprpoint + this.final_edupoint + this.final_bachelorpoint;

			var formData = new FormData();
      		 //var uploadfile = document.querySelector("#file");  // -- -- -- -- -- -- -- Upload File  -- -- -- -- -- -- -- --
			console.log('uploadfile ========== ');
			console.log(this.uploadfile);

			if(this.uploadfile==''){

               		 var f = new File([""], "empty"); //----------------- Error Handling of File ---------------
                	 formData.append('emptyfile',f.name);
			}else{
                	 formData.append('cvfile',this.uploadfile.files[0]);
            }
			formData.append('name',this.name);
			formData.append('email',this.email);
			formData.append('contact',this.contact);
			formData.append('occupationfield',this.occupation_name);
			formData.append('agerange',this.agerange_text);
			formData.append('ieltsscore',this.ielts_text);
			formData.append('expryears',this.experience_text);
			formData.append('eduqualification',this.education_text);
			formData.append('points',this.total_points);
			formData.append('worked_in_au',this.worked_in_au);
			formData.append('naati_exam',this.naati_exam);
			formData.append('spouse_skill',this.spouse_skill);

			axios.post('/storedata',formData,{ //------>>---->>---->>----->> Edit in server +"/calculator/storedata"

				headers:{
                    'Content-Type': 'multipart/form-data'
				}

			}).then(response => {

                this.showmessage = true;

                //console.log('- - - - - - - -response  - - - - - - ==== ');
                //console.log(response.data);
				//console.log(window.location);

                var getUrl = window.location;
                var baseUrl = getUrl.protocol + "//" + getUrl.host
				console.log('base ------- url === ');
				console.log(baseUrl);

               // window.alert('Our representative will contact you shortly.');
               // window.location=baseUrl+'/people'; //------>>---->>---->>----->> Edit in server +"/calculator/people"

            }).catch(error=> {
								console.log(error.response.data);
								this.errors.record( error.response.data );
				});

            }

	},
	mounted(){

		console.log('inside $$$$mounted ==== ');
		console.log(this.total_points);
	}

});