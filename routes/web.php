<?php

/*
|---------------------------------------------------------------------
| Web Routes
|---------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'PointsCalculatorController@index');
Route::post('/storedata', 'PointsCalculatorController@store');

Route::group(['middleware'=> ['auth']],function (){

        Route::get('/people', 'PointsCalculatorController@getpeople');
        Route::get('/people/{id}', 'PointsCalculatorController@single');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
