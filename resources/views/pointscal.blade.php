<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/pointscal.css">
</head>
<body>

<div class="container">
    <div class="row" style="display: flex; justify-content: center; margin-top: 20px">
        <div class="col-md-8">

            <div id="head-elements">
                <div class="logo">
                    <a href="http://ngsolutionsys.com/" target="_blank">
                        <img src="{{url('ngs-logo.png')}}" class="ng-logo img-responsive">
                    </a>
                </div>

                <div class="headingone">
                    <p>Australia Immigration Consultancy</p>
                </div>

                <div class="headingtwo">
                    <p>Points Calculator For Skilled Migration</p>
                </div>
            </div>
            <br>
            <div id="root" style="display: flex; flex-direction: column">
                <form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">

                    <div class="form-group" style="margin-top: 10px">
                        <label for="name" class="col-sm-5 control-label">Client's Name</label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" v-model="form.name">
        {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('name')"  v-text="form.errors.get('name')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="email" class="col-sm-5 control-label">Client's Email</label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email Address" v-model="form.email">
      {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('email')"  v-text="form.errors.get('email')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="contact" class="col-sm-5 control-label">Client's Contact</label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact Number" v-model="form.contact">
                        </div>
    {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('contact')"  v-text="form.errors.get('contact')">Name</span> {{--}}
                        <span class="col-sm-1 imp" >*</span>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="agerange" class="col-sm-5 control-label">Age Range</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="agerange" name="agerange" v-model="form.agerange">
                                <option value="0">Select Your Age Range</option>
                                <option value="25"> 18-24 years</option>
                                <option value="30"> 25-32 years</option>
                                <option value="25"> 33-39 years</option>
                                <option value="15"> 40-44 years</option>
                            </select>
   {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('agerange')"  v-text="form.errors.get('agerange')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>


                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="occupation" class="col-sm-5 control-label">Occupation</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="occupation" name="occupation"  v-model="form.occupation">
                                <option value="0">Select Occupation</option>
                                <option value="10"> Politician </option>
                                <option value="12"> Doctor </option>
                                <option value="14"> Engineer </option>
                                <option value="16"> Lawyer </option>
                                <option value="100"> Business Magnet </option>
                            </select>
      {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('occupation')"  v-text="form.errors.get('occupation')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="IELTS" class="col-sm-5 control-label">IELTS</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="IELTS" name="IELTS"  v-model="form.IELTS" >
                                <option value="0">IELTS Score All Band</option>
                                <option value="0"> ALL Band Min 6.0 </option>
                                <option value="10"> ALL Band Min 7.0 </option>
                                <option value="20"> ALL Band Min 8.0 </option>
                            </select>
     {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('occupation')"  v-text="form.errors.get('occupation')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>


                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="experience" class="col-sm-5 control-label">Skilled Employment in Nominated Occupation</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="experience" name="experience"  v-model="form.experience" >
                                <option value="0">Experience Time Range</option>
                                <option value="0"> Less than 3 years </option>
                                <option value="5"> 3-4 years </option>
                                <option value="10"> 5-7 years </option>
                                <option value="15"> 8-10 years </option>
                            </select>
     {{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('experience')"  v-text="form.errors.get('experience')">Name</span> {{--}}
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>

                    <div class="clearfix"></div>
                {{--    <div class="form-group" style="margin-top: 10px">
                        <label for="education" class="col-sm-5 control-label">Highest Academic Qualification Achieved</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="education" name="education"  v-model="form.education">
                                <option value="0">Highest Educational Level</option>
                                <option value="15"> Bachelor / Masters from any recognised Educational Institution in/outside AU </option>
                                <option value="20"> Doctorate degree from any recognised Educational Institution outside AU </option>
                            </select>
    --}}{{-- may not needed}}   <span class="text-danger" v-if="form.errors.has('education')"  v-text="form.errors.get('education')">Name</span> --}}{{--}}{{--
                        </div>
                        <span class="col-sm-1 imp" >*</span>
                    </div>--}}



                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="institution" class="col-sm-5 control-label">Academic Institution</label>
                        <div class="col-sm-6 every_field">
                            <select class="form-control" id="institution" name="institution"  v-model="form.institution">
                                <option value="0">Academic Institution</option>
                                <option value="Ahsanullah University of Science and Technology">Ahsanullah University of Science and Technology</option>
                               </select>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="worked_in_au" class="col-sm-5 control-label">Have you ever studied OR worked in AU?</label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="worked_in_au"
                                   name="worked_in_au" placeholder="If YES Give Details"  v-model="form.worked_in_au">
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="naati_exam" class="col-sm-5 control-label">Have you ever appeared for a NAATI exam?</label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="naati_exam"
                                   name="naati_exam" placeholder="If YES Give Details"  v-model="form.naati_exam">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label for="spouse" class="col-sm-5 control-label"> Do you want to assess your spouse skill? </label>
                        <div class="col-sm-6 every_field">
                            <input type="text" class="form-control" id="spouse"
                                   name="spouse" placeholder="If YES Give Details"  v-model="form.spouse">
                        </div>
                    </div>
       {{--------------------------------------------------------------------------------------}}
                    <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="education" class="page1 col-sm-5 control-label">Education</label>
                        <div class="col-sm-6 every_field">
                            <select id="education" name="education" class="form-control" placeholder="Phantasyland" @change="funEducation" v-model="form.education">
                                <option value="0" >Select Education Field</option>
                                <option value="Australian Institution" > Australian Institution</option>
                                <option value="7 Top Universities of BD"> 7 Top Universities of BD </option>
                                <option value="Others" > Others</option>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 10px">
                        <label class="page1 col-sm-5 control-label">Category</label>
                        <div class="col-sm-6 every_field">

                            <select id="educategory" class="form-control" name="educategory" placeholder="Anycity"  v-model="form.testedu">
                                <option value="0" selected> Select at first Education Field</option>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"></div>

     {{--------------------------------------------------------------------------------------}}
{{--
                    <div class="calculate_btn_div">
                            <button class="calculate_btn">Calculate Points</button>
                    </div>
--}}
                    <div class="calculated_points" >

                        <p> Calculated Point is: @{{
                        parseInt(form.agerange) +
                        parseInt(form.IELTS)+parseInt(form.experience)+
                        parseInt(form.testedu)
                        }}</p>

                    </div>
                    {{-----------------------------------------------------------------------------------}}

                   {{-- <div class="clearfix"></div>
                    <div class="form-group">
                        <label for="name">Project Name:</label>
                        <input type="text" class="form-control" id="name" name="name" v-model="form.name" >
                        <span class="text-danger" v-if="form.errors.has('name')"  v-text="form.errors.get('name')">There</span>
                    </div>

                    <div class="form-group">
                        <label for="description">Project Description</label>
                        <input type="text" class="form-control" id="description" name="description" v-model="form.description" >
                        <span  class="text-danger" v-if="form.errors.has('description')" v-text="form.errors.get('description')">There</span>
                    </div>
                    <button type="submit" :disabled="form.errors.any()" class="btn btn-success">Submit</button>--}}


                </form>
            </div>

        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.9/vue.js"></script>
<script src="/js/pointscal.js"></script>

<script>

  /*  jQuery(function($) {
        var educategories = {
           /!* 'Select Education Field At Firsttttttttt': { 'Selecting':500,'SelectMyboy':600},
            'Select Education Field At Firsttttttttt':0,  *!/
            'Select Education Field':{'Select at first Education Field':0},
            'Australian Institution': {'Select ':0, 'Australian University':20, 'Aus High School':10},
            '7 Top Universities of BD': {'Select ':0,'BUET':20, 'KUET':'20', 'RUET':'20'},
            'Others': {'Select ':0,'Masters':15,'Hons. / Diploma':10,'Under Graduate / HSC+':0},
        }


        $('#education').on('change',function () {

            var education = $(this).val(), lcns = educategories[education] || [];
            console.log('$(this).val()======'); console.log($(this).val());

            var html = $.map(lcns, function(lcn,key){
                console.log(lcn);
                return '<option value="' + lcn + '">' + key + '</option>'
            }).join('');

            document.getElementById('educategory').innerHTML = html
        });
    });*/

</script>

</body>
</html>


