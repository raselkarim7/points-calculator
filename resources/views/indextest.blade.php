@extends('layouts.app')
@section('content')
    <style>
        .ng-logo {
            width: 200px;
            margin-bottom: 10px;
        }
        .headingone{
            align-items: center;
            background: orange;
            color:white;
            padding: 10px;
            font-weight: 700;
            border-radius: 8px;
        }

        .headingone p{
            margin: 0;
        }

        .headingtwo{
            align-items: center;
            background: red;
            color:white;
            padding: 10px;
            font-weight: 700;
            border-radius: 8px;
        }

        .headingtwo p{
            margin: 0;
        }

        .wrapper{
            display: flex;
            justify-content: center;
            align-items: center;
        }


    </style>
    <div class="wrapper">
    <div id="root" style="background: #b4abff; padding: 20px">
        <div id="head-elements" style="margin-bottom: 30px">
            <div class="logo">
                <a href="http://ngsolutionsys.com/" target="_blank">
                    <img src="{{url('ngs-logo.png')}}" class="ng-logo img-responsive">
                </a>
            </div>

            <div class="headingone">
                <p>Australia Immigration Consultancy</p>
            </div>

            <div class="headingtwo">
                <p>Points Calculator For Skilled Migration</p>
            </div>
        </div>
        <form method="post"  @submit.prevent="onSubmit"  class="vertical">
            <div class="form-group row">
                <label for="name" class="col-sm-4 control-label">Client's Name:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name Here" v-model="name"  @keydown="errors.clear('name')">
                    <span class="help" style="color: red"  v-text="errors.get('name')"></span>
                </div>
            </div>

            {{--	<tr>
                    <td> <p>Client's Name:</p> </td>
                    <td>
                        <input type="text" class="form-control" name="name" v-model="name" placeholder="Name"  @keydown="errors.clear('name')">
                        <span class="help" style="color: red"  v-text="errors.get('name')"></span>
                    </td>
                </tr>--}}

            <div class="form-group row">
                <label for="email" class="col-sm-4"> Client's Email: </label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" name="email" v-model="email" placeholder="Email Address"  @keydown="errors.clear('email')">
                    <span class="help" style="color: red"  v-text="errors.get('email')"></span>
                </div>
            </div>


            <div class="form-group row">
                <label for="contact" class="col-sm-4"> Client's Contact: </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="contact" v-model="contact" placeholder="Contact Number"  @keydown="errors.clear('contact')">
                    <span class="help" style="color: red"  v-text="errors.get('contact')"></span>
                </div>
            </div>
            <div class="form-group row">
                <label for="occupation" class="col-sm-4"> Your Occupation: </label>
                <div class="col-sm-8">
                    <occupation v-on:occupationevent="occupationFun" > </occupation>
                    <span class="help"  style="color: red"  v-text="errors.get('occupationfield')"></span>
                </div>
            </div>


            <h3 style="color: #fff;"> Points Calculator</h3>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="age-range" class="col-sm-4"> Age Range: </label>
                <div class="col-sm-8">
                    <age-range   v-on:agerangeevent="agerangeFun"> </age-range>
                    <span class="help"  style="color: red"  v-text="errors.get('agerange')"></span>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="ielts" class="col-sm-4"> English (IELTS) :  </label>
                <div class="col-sm-8">
                    <ielts  v-on:ieltsevent="ieltsFun"> </ielts>
                    <span class="help"  style="color: red"  v-text="errors.get('ieltsscore')"></span>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="experience" class="col-sm-4"> Skilled Employment<br>  in Nominated Occupation:  </label>
                <div class="col-sm-8">
                    <experience  v-on:experienceevent="experienceFun"> </experience>
                    <span class="help"  style="color: red"  v-text="errors.get('expryears')"></span>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="education" class="col-sm-4">  Highest Academic Qualification Achieved:  </label>
                <div class="col-sm-8">
                    <education  v-on:educationevent="educationFun"> </education>
                    <span class="help"  style="color: red"  v-text="errors.get('eduqualification')"></span>

                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="forbachelor" class="col-sm-4"> Select Academic Instituion <br> if only your Education="Bachelor"</label>
                <div class="col-sm-8">
                    <select v-model="bachelorpoint" @v-on:change="bachelorFun(this)" class="form-control" id="forbachelor">
                        <option disabled value=""> Academic Institutions </option>
                        <option v-for="opt in institutionlist" @v-bind:value=opt> @{{ opt.text }}</option>
                    </select>
                <!-- <p>  Correspoing value of second selection = {{--	@{{ typeof bachelorpoint }}--}} </p>  -->
                </div>
            </div>

            <div class="clearfix"></div>
            <h2 style="color: #fff">
                Calculated Approximate Point =   @{{	 parseInt( final_agepoint )
													+ parseInt( final_ieltspoint )
													+ parseInt( final_exprpoint )
													+ parseInt( final_edupoint )
													+ parseInt( final_bachelorpoint )
											 }}
            </h2>


            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="worked_in_au" class="col-sm-4"> Have you ever <br> studied OR worked in AU? </label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="worked_in_au" v-model="worked_in_au" placeholder="If YES Give Details."> </textarea>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="naati_exam" class="col-sm-4"> Have you ever appeared <br> for a NAATI exam? </label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="naati_exam" v-model="naati_exam" placeholder="If YES Give Details."> </textarea>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="spouse_skill" class="col-sm-4"> Do you want to assess your spouse skill? </label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="spouse_skill" v-model="spouse_skill" placeholder="If YES Give Details."> </textarea>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-group row">
                <label for="file" class="col-sm-4">Upload Resume </label>
                <div class="col-sm-8">
                    <div style="font-size: 16px">
                        <div class="form-group" v-if="!cvfile">
                            <span>Select a .pdf or .doc </span>
                            <input class="form-control" aria-describedby="fileHelp" id="file" name="file" type="file" @change="onFileChange">
                        </div>
                        <div v-else>
                            <p :src="cvfile" >@{{cvfilename }}</p>
                            <a href="#" @click="removeFile">Remove File</a>
                        </div>
                    </div>

                </div>
            </div>

            <button class="btn btn-default" name="submit"  id="submit" type ="submit" style="float: right;">Submit</button>
        </form>
    </div>

    </div>



@endsection
