@extends('layouts.app')
@section('content')

    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">The List of People</h1>
            <p class="lead blog-description">Those who registered, their list are given below.</p>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-sm-8 blog-main">




                @foreach ($users as $user)
                    <h4>
                        <li>
                            <a href="{{url('people',$user->id)}}">
                                {{  $user->name}}
                                <span> : {{ $user->points}}</span>
                            </a>
                        </li>
                    </h4>

                @endforeach

                <br><br><br>

                {{--<nav class="blog-pagination">
                    <a class="btn btn-outline-primary" href="#">Older</a>
                    <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
                </nav>--}}

            </div><!-- /.blog-main -->






        {{--  <div class="col-sm-3 offset-sm-1 blog-sidebar">
              <div class="sidebar-module sidebar-module-inset">
                  <h4>About</h4>
                  <p>Etiam porta <em>sem malesuada magna</em> mollis   consectetur.</p>
              </div>
              <div class="sidebar-module">
                  <h4>Archives</h4>
                  <ol class="list-unstyled">
                      <li><a href="#">March 2014</a></li>
                      <li><a href="#">April 2014</a></li>
                      <li><a href="#">May 2014</a></li>

                  </ol>
              </div>
              <div class="sidebar-module">
                  <h4>Elsewhere</h4>
                  <ol class="list-unstyled">
                      <li><a href="#">GitHub</a></li>
                      <li><a href="#">Twitter</a></li>
                      <li><a href="#">Facebook</a></li>
                  </ol>
              </div>
          </div>--}}<!-- /.blog-sidebar -->

        </div><!-- /.row -->

    </div><!-- /.container -->

@endsection
