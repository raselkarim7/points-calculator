@extends('layouts.app')

@section('content')
    <style>
        .blog-main{
            font-size: 16px;

        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding: 10px;

        }
    </style>

    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">Name: {{$showsingle->name}}</h1>
            <p class="lead blog-description">His full biodatas are given below.</p>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-sm-10 blog-main">


                <table class="table-bordered table">
                    <tbody>
                    <tr>
                        <th> Name:  </th>
                        <td>  {{  $showsingle->name }}   </td>
                    </tr>
                    <tr>
                        <th> Email:  </th>
                        <td>  {{  $showsingle->email }}   </td>
                    </tr>
                    <tr>
                        <th> Contact:  </th>
                        <td>  {{  $showsingle->contact }}   </td>
                    </tr>
                    <tr>
                        <th> Occupation:  </th>
                        <td>  {{  $showsingle->occupationfield }}   </td>
                    </tr>
                    <tr>
                        <th> Age Range:  </th>
                        <td>  {{  $showsingle->agerange }}   </td>
                    </tr>

                    {{--   'name', 'email', 'contact', 'occupationfield', 'agerange',
                       'ieltsscore','expryears','eduqualification','worked_in_au','naati_exam','spouse_skill','cvfile'--}}

                    <tr>
                        <th> IELTS Score:  </th>
                        <td>  {{  $showsingle->ieltsscore }}   </td>
                    </tr>
                    <tr>
                        <th> Experience Years:  </th>
                        <td>  {{  $showsingle->expryears }}   </td>
                    </tr>
                    <tr>
                        <th> Education Qualification:  </th>
                        <td>  {{  $showsingle->eduqualification }}   </td>
                    </tr>
                    <tr style="background: greenyellow">
                        <th> Total Points:  </th>
                        <td>  {{  $showsingle->points }}   </td>
                    </tr>
                    <tr>
                        <th> Worked In AU:  </th>
                        <td>  {{  $showsingle->worked_in_au }}   </td>
                    </tr>
                    <tr>
                        <th> Naati Exam:  </th>
                        <td>  {{  $showsingle->naati_exam }}   </td>
                    </tr>
                    <tr>
                        <th> Spouse Skill:  </th>
                        <td>  {{  $showsingle->spouse_skill }}   </td>
                    </tr>
                    <tr>
                        <th> Show Resume:  </th>

                        @if($showsingle->cvfile!='No file uploaded')
                            <td> <a href="{{url('/uploadedcv',$showsingle->cvfile)}}" target="_blank"> {{ ' Click To See' }} </a>  </td>
                        @else
                            <td> <p> No file uploaded </p>  </td>
                        @endif
                    </tr>

                    </tbody>
                </table>



            </div><!-- /.blog-main -->

        {{--

        --}}<!-- /.blog-sidebar -->

        </div><!-- /.row -->


      {{--  <nav class="blog-pagination">
            <a class="btn btn-outline-primary" href="#">Older</a>
            <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
        </nav>--}}

    </div><!-- /.container -->
@endsection