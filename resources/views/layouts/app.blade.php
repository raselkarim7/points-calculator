
<!DOCTYPE html>
<html lang="en">
<head xmlns:v-on="http://www.w3.org/1999/xhtml" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Blog Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/blog.css') }} " rel="stylesheet">
    <style>
        .blog-nav{
                padding: 10px;
                font-size: 20px;
        }
        .blog-nav li a:hover{
                background: #0603f0;
                color: #fff;
        }
    </style>
</head>

<body>

<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav" style="display: flex;  justify-content: space-between; align-items: center" >
            <div style="display: flex; flex-direction: row">
            <li class="">
                <a class="nav-link {{ request()->is('people') ? 'active' : '' }}" href="{{url('/people')}}">People</a>
            </li>

            <li class="">
                <a class="nav-link {{ request()->is('/') ? 'active' : '' }}" href="{{url('/')}}">Points Calculator</a>
            </li>
            </div>
            {{--
               <a class="nav-link" href="#">Press</a>
               <a class="nav-link" href="#">New hires</a>
               <a class="nav-link" href="#">About</a>--}}
            <div>
            @if(\Auth::check())
            <li >
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();" style="color: white">Logout</a>
                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;"> {{ csrf_field() }} </form>
            </li>
             @endif
            </div>
        </nav>
        <nav></nav>
    </div>
    <div>
        <p>&nbsp;</p>
    </div>
</div>
@yield('content')



<footer class="blog-footer">
    <p>This page is designed for Australia Immigration Consultancy.</p>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.13/dist/vue.js"></script>

@yield('calculator-js')


{{--<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" ></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="https://v4-alpha.getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>--}}
</body>
</html>
